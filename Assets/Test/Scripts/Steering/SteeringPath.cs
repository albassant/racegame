﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SteeringPath
{
    private List<Vector3> pathPoints = new List<Vector3>();

    public float TotalPathLength { get; private set; }

    public SteeringPath(Path path)
    {
        Node headNode = path.nodes[0];
        Node curNode = headNode;

        int counter = 0;
        while (counter < path.nodes.Count)
        {
            pathPoints.Add(curNode.transform.position);
             curNode = curNode.next;
            counter++;
        }
        pathPoints.Add(headNode.transform.position);

        for (int i = 1; i < pathPoints.Count; i++)
        {
            TotalPathLength += Vector3.Distance(pathPoints[i-1], pathPoints[i]);
        }
    }

    public float GetPathParameter(Vector3 worldPosition)
    {
        int closestIdx = 0;
        Vector3 closestPoint = pathPoints[0];
        float minDistance = Vector3.Distance(worldPosition, closestPoint);

        for (int i = 1; i < pathPoints.Count; i++)
        {
            Vector3 cps = GetClosestPointSegment(pathPoints[i - 1], pathPoints[i], worldPosition);
            float distance = Vector3.Distance(worldPosition, cps);

            if (distance < minDistance)
            {
                minDistance = distance;
                closestIdx = i - 1;
                closestPoint = cps;
            }
        }

        float param = 0f;
        for (int i = 0; i <= closestIdx; i++)
        {
            if (i == closestIdx)
            {
                float distanceFromCurPos = Vector3.Distance(pathPoints[i], closestPoint);
                param += distanceFromCurPos;
            }
            else
            {
                param += Vector3.Distance(pathPoints[i], pathPoints[i + 1]);
            }
        }
        
        return param;
    }

    public Vector3 GetWorldPosition(float param)
    {
        float curPathLength = 0f;
        for (int i = 1; i < pathPoints.Count; i++)
        {
            float pathLengthToBe = curPathLength + Vector3.Distance(pathPoints[i - 1], pathPoints[i]);
            if (pathLengthToBe > param)
            {
                float delta = param - curPathLength;
                Vector3 position = pathPoints[i - 1] + (pathPoints[i] - pathPoints[i - 1]).normalized * delta;
                return position;
            }
            curPathLength = pathLengthToBe;
        }
        return pathPoints[0];
    }

    private Vector3 GetClosestPointSegment(Vector3 a, Vector3 b, Vector3 c)
    {
        Vector3 ab = b - a;
        float t = Vector3.Dot(c - a, ab) / Vector3.Dot(ab, ab);
        t = Mathf.Clamp(t, 0.0f, 1.0f);

        return a + t * ab;
    }  
}
