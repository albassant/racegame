﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SteeringBehaviour
{
    protected SteeringContext context;
    public SteeringBehaviour(SteeringContext context)
    {
        this.context = context;
    }
    public abstract Vector3 GetDesiredDirection(Vehicle vehicle);
}

public class PathFollowingSteering : SteeringBehaviour
{
    public PathFollowingSteering(SteeringContext context) : base(context)
    {
    }

    public override Vector3 GetDesiredDirection(Vehicle vehicle)
    {
        const float lookAhead = 10.0f;

        float p = context.steeringPath.GetPathParameter(vehicle.transform.position);
        Vector3 posOnPath = context.steeringPath.GetWorldPosition(p + lookAhead);
        return (posOnPath - vehicle.transform.position).normalized;
    }

    public float GetPathProgress(Vehicle vehicle)
    {
        float current = context.steeringPath.GetPathParameter(vehicle.transform.position);
        float total = context.TotalSteeringPathLength;
        return current / total;
    }
}

public class AvoidOthersSteering : SteeringBehaviour
{
    public float threshold = 15f;

    public AvoidOthersSteering(SteeringContext context) : base(context)
    {
    }

    public override Vector3 GetDesiredDirection(Vehicle vehicle)
    {
        Vector3 desiredDirection = Vector3.zero;

        foreach (var target in context.vehicles)
        {
            if (target == vehicle)
                continue;

            var direction = vehicle.transform.position - target.transform.position;
            var distance = direction.sqrMagnitude;

            if (distance < threshold * threshold)
            {
                desiredDirection += direction;
            }
        }

        return desiredDirection;
    }
}

public class BlendedSteering : SteeringBehaviour
{
    PathFollowingSteering pathFollow;
    AvoidOthersSteering avoidOthers;

    public float GetPathProgress(Vehicle vehicle)
    {
        return pathFollow.GetPathProgress(vehicle);
    }

    public BlendedSteering(SteeringContext context) : base(context)
    {
        pathFollow = new PathFollowingSteering(context);
        avoidOthers = new AvoidOthersSteering(context);
    }

    public override Vector3 GetDesiredDirection(Vehicle vehicle)
    {
        return pathFollow.GetDesiredDirection(vehicle) * 0.9f + avoidOthers.GetDesiredDirection(vehicle) * 0.1f;
    }
}
