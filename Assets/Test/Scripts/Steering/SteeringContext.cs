﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringContext : MonoBehaviour
{
    public List<Vehicle> vehicles = new List<Vehicle>();
    public SteeringPath steeringPath;

    private static SteeringContext instance;
    public static SteeringContext Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<SteeringContext>();
            return instance;
        }
    }

    public float TotalSteeringPathLength { get { return steeringPath.TotalPathLength; } }

    public void InitSteeringPath(Path path)
    {
        steeringPath = new SteeringPath(path);
    }

    public void AddVehicle(Vehicle vehicle)
    {
        vehicles.Add(vehicle);
    }

    public void RemoveVehicle(Vehicle vehicle)
    {
        vehicles.Remove(vehicle);
    }
}
