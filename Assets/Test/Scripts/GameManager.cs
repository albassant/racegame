﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{
    [SerializeField] Vehicle playerPrefab;
    [SerializeField] Transform startPoint;
    [SerializeField] TextMesh startText;

    public System.Action<Vehicle> OnPlayerInstantiated;
    public System.Action<Vehicle> OnGameOver;

    public int NumberOfPlayers = 8;

    public List<Vehicle> Players = new List<Vehicle>();

    private bool isGameOver = false;

	void Start()
    {
        Path path = FindObjectOfType<Path>();
        SteeringContext.Instance.InitSteeringPath(path);

        StartCoroutine(CountdownCoroutine()); 
	}

    IEnumerator CountdownCoroutine()
    {
        int counter = 3;

        while (counter >= 0)
        {
            startText.text = counter == 0 ? "Go!" : counter.ToString();
            yield return new WaitForSeconds(1f);
            counter--;
        }

        startText.text = "";

        LoadPlayers();
    }

    void LoadPlayers()
    {
        List<PlayerConfig> playersConfigs = new List<PlayerConfig>();
        playersConfigs.AddRange(ConfigManager.Instance.PlayersConfigs);

        List<PlayerConfig> chosenPlayers = new List<PlayerConfig>();
        
        int counter = 0;
        while (counter < NumberOfPlayers)
        {
            int idx = Random.Range(0, playersConfigs.Count);
            chosenPlayers.Add(playersConfigs[idx]);
            playersConfigs.RemoveAt(idx);

            counter++;
        }

        chosenPlayers.Sort((x, y) => x.Velocity.CompareTo(y.Velocity));

        StartCoroutine(InstantiatePlayersCoroutine(chosenPlayers));
    }

    IEnumerator InstantiatePlayersCoroutine(List<PlayerConfig> chosenPlayers)
    {
        float delay = ConfigManager.Instance.GameConfig.playersInstantiationDelay / 1000f;

        int i = 0;
        while (i < chosenPlayers.Count)
        {
            Vehicle player = GameObject.Instantiate<Vehicle>(playerPrefab);
            player.transform.position = startPoint.position;
            player.Info.SetupPlayer(chosenPlayers[i]);
            player.Info.RatingPosition = i;
            player.OnLapCompleted += OnVehicleCompletedLap;
            player.OnFinishedRace += OnVehicleFinishedRace;

            Players.Add(player);

            if (OnPlayerInstantiated != null)
                OnPlayerInstantiated(player);

            yield return new WaitForSeconds(delay);

            i++;
        }
    }

    void OnVehicleCompletedLap(Vehicle vehicle)
    {
        if (vehicle.Info.CurrentLap == ConfigManager.Instance.GameConfig.lapsNumber)
        {
            vehicle.Stop();
        }
    }

    int finishedCounter = 0;
    void OnVehicleFinishedRace(Vehicle vehicle)
    {
        vehicle.OnLapCompleted -= OnVehicleCompletedLap;
        vehicle.OnFinishedRace -= OnVehicleFinishedRace;

        finishedCounter++;

        if (finishedCounter == NumberOfPlayers)
        {
            EndGame();
            finishedCounter = 0;
        }
    }

    void UpdatePlayersRatings()
    {
        if (Players.Count > 1)
        {
            Players.Sort((x, y) =>
            {
                return y.GetProgress().CompareTo(x.GetProgress());
            });

            for (int i = 0; i < Players.Count; i++)
            {
                if (!Players[i].FinishedRace)
                    Players[i].Info.RatingPosition = i;
            }
        }
    }

    void Update()
    {
        if (!isGameOver)
            UpdatePlayersRatings();
    }

    public void EndGame()
    {
        isGameOver = true;
        foreach (var p in Players)
            Debug.Log(p.Info.Name + " rating pos " + p.Info.RatingPosition);

        Vehicle winner = Players.OrderBy(p => p.Info.RatingPosition).First();
        if (OnGameOver != null)
            OnGameOver(winner);
    }

    public void StartGame()
    {
        foreach (var p in Players)
        {
            GameObject.Destroy(p.gameObject);
        }
        Players.Clear();

        isGameOver = false;
        StartCoroutine(CountdownCoroutine());
    }
}
