﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GizmosUtils
{
	public static void DrawText(GUISkin guiSkin, string text, Vector3 position, Color? color = null, int fontSize = 0, float yOffset = 0)
	{
		#if UNITY_EDITOR
		var prevSkin = GUI.skin;
		if (guiSkin == null)
			Debug.LogWarning("editor warning: guiSkin parameter is null");
		else
			GUI.skin = guiSkin;

		GUIContent textContent = new GUIContent(text);

		GUIStyle style = (guiSkin != null) ? new GUIStyle(guiSkin.GetStyle("Label")) : new GUIStyle();
		if (color != null)
			style.normal.textColor = (Color)color;
		if (fontSize > 0)
			style.fontSize = fontSize;

		Vector2 textSize = style.CalcSize(textContent);
		Vector3 screenPoint = Camera.current.WorldToScreenPoint(position);

		if (screenPoint.z > 0) // checks necessary to the text is not visible when the camera is pointed in the opposite direction relative to the object
		{
			var worldPosition = Camera.current.ScreenToWorldPoint(new Vector3(screenPoint.x - textSize.x * 0.5f, screenPoint.y + textSize.y * 0.5f + yOffset, screenPoint.z));
			UnityEditor.Handles.Label(worldPosition, textContent, style);
		}
		GUI.skin = prevSkin;
		#endif
	}
}

public static class ConversionUtils
{
    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(1, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(3, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(5, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color(r, g, b, 255) / 255.0f;
    }
}