﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] PlayerRatingCellController ProgressCellPrefab;
    [SerializeField] RectTransform CellsLayoutTransform;
    [SerializeField] WinnerInfoPopupController WinnerPopup;
    [SerializeField] GameObject ReplayButton;

    private GameManager gameManager;

    private Dictionary<Vehicle, Transform> ratingCells = new Dictionary<Vehicle, Transform>();

    void Start()
    {
        WinnerPopup.gameObject.SetActive(false);
        ReplayButton.SetActive(false);

        gameManager = FindObjectOfType<GameManager>();
        gameManager.OnPlayerInstantiated += CreatePlayerCell;
        gameManager.OnGameOver += HandleGameOver;
    }

    void CreatePlayerCell(Vehicle player)
    {
        PlayerRatingCellController cell = GameObject.Instantiate<PlayerRatingCellController>(ProgressCellPrefab);
        cell.transform.SetParent(CellsLayoutTransform.transform);
        cell.Player = player;

        ratingCells.Add(player, cell.transform);
    }

    void HandleGameOver(Vehicle winner)
    {
        WinnerPopup.Show(winner.Info);
        ReplayButton.SetActive(true);
    }

    public void RestartGame()
    {
        ReplayButton.SetActive(false);

        foreach (var c in ratingCells)
        {
            Destroy(c.Value.gameObject);
        }
        ratingCells.Clear();

        gameManager.StartGame();
    }

    void Update()
    {
        foreach (var p in gameManager.Players)
        {
            if (ratingCells.ContainsKey(p) && ratingCells[p].GetSiblingIndex() != p.Info.RatingPosition)
                ratingCells[p].SetSiblingIndex(p.Info.RatingPosition);
        }
    }

    void OnDestroy()
    {
        if (gameManager != null)
        {
            gameManager.OnPlayerInstantiated -= CreatePlayerCell;
            gameManager.OnGameOver -= HandleGameOver;
        }
    }
}
