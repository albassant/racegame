﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRatingCellController : MonoBehaviour
{
    [SerializeField]
    RawImage playerIcon;
    [SerializeField]
    Text playerName;
    [SerializeField]
    Slider playerProgress;

    public Vehicle Player { private get; set; }

    void Start()
    {
        if (Player != null)
        {
            playerName.text = Player.Info.Name;
            playerProgress.value = 0f;
            Player.Info.GetIcon((texture) => playerIcon.texture = texture);
        }
    }

    void Update()
    {
		if (Player != null)
        {
            playerProgress.value = Player.GetProgress();
        }
	}
}
