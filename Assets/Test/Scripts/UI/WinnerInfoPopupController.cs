﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinnerInfoPopupController : MonoBehaviour
{
    [SerializeField] RawImage playerIcon;
    [SerializeField] Text playerName;

    void Start()
    {
        this.transform.localScale = Vector3.zero;
    }

    public void Show(VehicleInfo playerInfo)
    {
        this.gameObject.SetActive(true);
        
        this.playerName.text = playerInfo.Name;
        playerInfo.GetIcon((texture) => playerIcon.texture = texture);

        StartCoroutine(AnimationCoroutine(Vector3.zero, Vector3.one));
    }

    public void Hide()
    {
        StartCoroutine(AnimationCoroutine(Vector3.one, Vector3.zero, () => 
        {
            this.playerName.text = "";
            playerIcon.texture = null;
        }));
    }

    IEnumerator AnimationCoroutine(Vector3 from, Vector3 to, System.Action callback = null)
    {
        float time = 0.3f;
        float t = 0f;
        while (t <= time)
        {
            t += Time.deltaTime;
            this.transform.localScale = Vector3.Lerp(from, to, t / time);
            
            yield return null;
        }

        if (callback != null)
            callback();
    }
}
