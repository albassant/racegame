﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VehicleInfo))]
public class Vehicle : MonoBehaviour 
{
    [SerializeField] WheelCollider[] wheels;
    [SerializeField] GameObject[] frontWheelModels;
    [SerializeField] Renderer mainCarRenderer;
    [SerializeField] TextMesh nameText;

    public VehicleInfo Info;

    public bool FinishedRace { get; private set; }

    private SteeringBehaviour rootSteering;

    public System.Action<Vehicle> OnLapCompleted;
    public System.Action<Vehicle> OnRatingPositionChanged;
    public System.Action<Vehicle> OnFinishedRace;

	void Start()
	{
        SteeringContext.Instance.AddVehicle(this);
        rootSteering = new BlendedSteering(SteeringContext.Instance);
        FinishedRace = false;

        mainCarRenderer.material.color = Info.SkinColor;
        nameText.text = Info.Name;
    }

	void FixedUpdate()
	{
        if (!FinishedRace)
        {
            Vector3 direction = rootSteering.GetDesiredDirection(this);
            if (direction.sqrMagnitude > 0.1f * 0.1f)
            {
                direction.Normalize();
                SteerTo(direction);
            }
        }
    }

    public void Stop()
    {
        FinishedRace = true;
        foreach (var w in wheels)
        {
            w.brakeTorque =Info.MaxAcceleration * 0.95f;
        }

        if (OnFinishedRace != null)
            OnFinishedRace(this);
    }

    public void SteerTo(Vector3 direction)
    {
        float dot = Vector3.Dot(direction, transform.forward);
        Vector3 cross = Vector3.Cross(direction, transform.forward);

        float angle = Vector3.Angle(direction, transform.forward);
        angle = Mathf.Clamp(angle, -55f, 55f);

        if (cross.y > 0.0f)
            angle = -angle;

        if (dot > 0.99f)
            angle = 0f;

        foreach (var w in wheels)
        {
            w.motorTorque = Info.MaxAcceleration;
            w.steerAngle = angle;
        }

        foreach (var w in frontWheelModels)
        {
            w.transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
        }
    }

    public float GetProgress()
    {
        int totalLaps = ConfigManager.Instance.GameConfig.lapsNumber;
        float currentLapProgress = GetCurrentLapProgress() / (float)totalLaps;
        float completeLapsProgress = Info.CurrentLap / (float)totalLaps;

        return completeLapsProgress + currentLapProgress;
    }

    float prev;
    public float GetCurrentLapProgress()
    {
        float cur = (rootSteering as BlendedSteering).GetPathProgress(this);
        if (prev < cur)
        {
            prev = cur;
        }
        else if (prev - cur > 0.5f)
        {
            Info.CurrentLap++;

            if (OnLapCompleted != null)
                OnLapCompleted(this);

            prev = cur;
        }

        return cur;
    }

    void OnDestroy()
    {
        if (SteeringContext.Instance != null)
            SteeringContext.Instance.RemoveVehicle(this);
    }
}
