﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

public class ConfigManager
{
    private static ConfigManager instance;

    private ConfigManager()
    {
        LoadJSON();
    }

    public static ConfigManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new ConfigManager();
            }
            return instance;
        }
    }

    private Configuration config;

    private void LoadJSON()
    {
        string path = Application.dataPath + "/StreamingAssets/" + "Data.txt";
        string jsonString = File.ReadAllText(path);
        config = JsonConvert.DeserializeObject<Configuration>(jsonString);
    }

    public List<PlayerConfig> PlayersConfigs { get { return config.players; } }
    public GameConfiguration GameConfig { get { return config.gameConfiguration; } }
}

public class Configuration
{
    public GameConfiguration gameConfiguration;
    public List<PlayerConfig> players;
}

public class GameConfiguration
{
    public int lapsNumber;
    public int playersInstantiationDelay;
}

public class PlayerConfig
{
    public string Name;
    public int Velocity;
    public string Color;
    public string Icon;

    public Color SkinColor { get { return ConversionUtils.HexToColor(Color); }}
}