﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]

public class Path : MonoBehaviour 
{
	public List<Node> nodes;

	#region Editor methods
	[HideInInspector]
	public Node lastModifiedNode;
	[HideInInspector]
	public int nodesCounter = 0;
	public LayerMask roadLayer;

	public Node AddNode(Vector3 position)
	{
		Node newNode = GameObject.Instantiate(Resources.Load("Node") as GameObject).GetComponent<Node>();
		newNode.transform.position = position;
		newNode.transform.parent = this.transform;
		newNode.name = nodesCounter.ToString();
		nodesCounter++;

		if (lastModifiedNode != null)
		{
			ConnectNodes(lastModifiedNode, newNode);
		}
		else
		{
			lastModifiedNode = newNode;
		}

		nodes.Add(newNode);
		return newNode;
	}

	public void RemoveNode(Node node)
	{
		if (node.previous != null)
			node.previous.next = node.next;

		if (node.next != null)
			node.next.previous = node.previous;

		if (node == lastModifiedNode)
			lastModifiedNode = node.previous;

		nodes.Remove(node);
	}

	public void ConnectNodes(Node curNode, Node nextNode)
	{
		if (curNode.next != null)
			curNode.next.previous = null;
		
		curNode.next = nextNode;

		if (nextNode.previous != null)
			nextNode.previous.next = null;
			
		nextNode.previous = curNode;
		lastModifiedNode = nextNode;
	}

    public void SetStartNode(Node node)
    {
        int index = nodes.IndexOf(node);
        Node first = nodes[0];
        nodes[index] = first;
        nodes[0] = node;
    }
	#endregion
}
