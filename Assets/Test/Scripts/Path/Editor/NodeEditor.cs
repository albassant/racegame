﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Node))]
[CanEditMultipleObjects]
public class NodeEditor : Editor 
{
	private Node node;

	void OnEnable()
	{
		node = target as Node;
	}

	//public override void OnInspectorGUI() {}

	void OnSceneGUI()
	{
		Handles.BeginGUI();
		this.CheckSelectedObjects();
		Handles.EndGUI();
	}

	void CheckSelectedObjects()
	{
		if (Selection.gameObjects.Length == 2)
		{
			foreach (GameObject go in Selection.gameObjects)
			{
				if (go.GetComponent<Node>() == null)
				{
					// some of the selected objects is not a node, stop handling selection
					return;
				}
			}

			GUILayout.BeginArea(new Rect(Screen.width - 200, Screen.height - 100, 180, 40));
			this.ConnectButton(Selection.gameObjects[0], Selection.gameObjects[1]);
			this.ConnectButton(Selection.gameObjects[1], Selection.gameObjects[0]);
			GUILayout.EndArea();
		}
		else if (Selection.gameObjects.Length == 1)
		{
			GUILayout.BeginArea(new Rect(Screen.width - 200, Screen.height - 80, 180, 40));
			if (GUILayout.Button("Disconnect"))
			{
				node.Disconnect();
			}
			GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(20, Screen.height - 80, 180, 40));
            if (GUILayout.Button("Set Start Node"))
            {
                Path path = node.GetParentPath();
                if (path != null)
                {
                    path.SetStartNode(this.node);
                }
            }
            GUILayout.EndArea();
        }
	}

	void ConnectButton(GameObject nodeA, GameObject nodeB)
	{
		Node nodeAComp = nodeA.GetComponent<Node>();
		Node nodeBComp = nodeB.GetComponent<Node>();

		if (GUILayout.Button("Link " + nodeA.name + " -> " + nodeB.name))
		{
			nodeAComp.GetParentPath().ConnectNodes(nodeAComp, nodeBComp);
		}
	}
}
