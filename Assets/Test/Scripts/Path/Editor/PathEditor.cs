﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Path))]
public class PathEditor : Editor 
{
	private Path path;

	void OnEnable()
	{
		path = target as Path;
	}

	public override void OnInspectorGUI()
	{
		GUILayout.BeginVertical("box");
		GUILayout.Label("Ctrl + left click on the road to add nodes");
		GUILayout.Label("Select any 2 nodes for connection options");

		GUILayout.Space(20);

		GUILayout.Label("Nodes naming counter: " + path.nodesCounter);
		if (GUILayout.Button("Reset nodes naming counter"))
			path.nodesCounter = 0;

		GUILayout.EndVertical();

		DrawDefaultInspector();
	}

	void OnSceneGUI()
	{
		Event ev = Event.current;
		bool editModeOn = ev.control;

		if (editModeOn)
		{
			Selection.activeGameObject = path.gameObject;
		}

		if (editModeOn && ev.type == EventType.mouseDown && ev.button == 0)
		{
			Vector3? clickPosition = GetWorldClickPosition(ev.mousePosition);
			if (clickPosition != null) 
			{
				path.AddNode(clickPosition.Value);
			}
		}
	}

	private Vector3? GetWorldClickPosition(Vector2 mousePosition)
	{
		Ray worldray = HandleUtility.GUIPointToWorldRay(mousePosition);

		RaycastHit hit;
		if (Physics.Raycast(worldray, out hit, 1000.0f, path.roadLayer))
		{
			return hit.point;
		}

		return null;
	}
}
