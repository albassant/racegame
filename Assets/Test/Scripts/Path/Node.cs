﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Node : MonoBehaviour
{
	public Node next;
	public Node previous;

	#region Editor methods
	public virtual void OnDrawGizmos() 
	{
		float yOffset = 26;
		int fontSize = 12;
		GizmosUtils.DrawText(GUI.skin, this.name, transform.position, Color.red, fontSize, yOffset);

		Gizmos.color = Color.white;
		float radius = 2f;
		Gizmos.DrawSphere(transform.position, radius);

		// draw connection arrow
		if (next != null && next != this)
		{   
			Vector3 direction = next.transform.position - transform.position;
			Vector3 position = transform.position + direction.normalized;
			direction = direction - direction.normalized * 2 * radius;

			Gizmos.color = Color.yellow;
			Gizmos.DrawRay(position, direction);

			float arrowHeadAngle = 20f;
			float arrowHeadLength = 2f;

			Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * Vector3.forward;
			Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * Vector3.forward;
			Gizmos.DrawRay(position + direction, right * arrowHeadLength);
			Gizmos.DrawRay(position + direction, left * arrowHeadLength);
		}
	}

	public void Disconnect()
	{
		if (this.next != null)
			this.next.previous = null;
		this.next = null;

		if (this.previous != null)
			this.previous.next = null;
		this.previous = null;
	}

	protected void OnDestroy()
	{
		if (Application.isEditor && !Application.isPlaying) 
		{
			Path path = this.GetParentPath();
			if (path != null)
			{
				path.RemoveNode(this);
			}
		}
	}

	public Path GetParentPath()
	{
		return this.GetComponentInParent<Path>();
	}
	#endregion
}
