﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleInfo : MonoBehaviour
{
    public string Name { get; private set; }
    
    public int RatingPosition;
    public int CurrentLap;

    public float MaxAcceleration = 20000f;
    public Color SkinColor;

    private string iconURL;
    private Texture iconTexture;

    public void SetupPlayer(PlayerConfig config)
    {
        MaxAcceleration = config.Velocity * 500f;
        SkinColor = config.SkinColor;
        Name = config.Name;
        iconURL = config.Icon;
    }

    IEnumerator LoadIconCoroutine(System.Action<Texture> callback)
    {
        WWW www = new WWW(iconURL);
        yield return www;

        iconTexture = www.texture;
        callback(iconTexture);
    }

    public void GetIcon(System.Action<Texture> callback)
    {
        if (iconTexture == null)
            StartCoroutine(LoadIconCoroutine(callback));
        else
            callback(iconTexture);
    }
}
